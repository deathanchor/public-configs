syntax on
set ignorecase
set smartcase

" this is the macro for new perl functions
nmap <F3> bdwisub pA {=pod=over 4=item pAI<Arguements>: I<Returns>: =back=cutmy( @args ) = @_;return 1;} ################################################## END pi6k

" this macro folds on the previous { curly
nmap <F4> ?{jvk%kzf

" These are for commenting in perl and uncommenting
nmap <F8> V: s/\v^(\s*)#+ ?/\1/g0w:nohls
vmap <F8> : s/\v^(\s*)#+ ?/\1/g0w:nohls
"nmap <F8> V: s/\v^(\s*)#[# ]{-}(\s{2,})?/\1\2/g0w:nohls
"vmap <F8> : s/\v^(\s*)#[# ]{-}\s{2,})?/\1\2/g0w:nohls
nmap <F9> V: s/\v^(\s*)/\1# /g:nohls 
vmap <F9> : s/\v^(\s*)/\1# /g:nohls

" move commands while in insert
imap <C-e> A
imap <C-a> I

nmap <C-@> 0: s/\v(\S+).*/\1/g:nohls$
vmap <C-@> 0: s/\v(\S+).*/\1/g:nohls$

" to retrieve a macro from a register ie: q use "qp
" that will print the macro to the screen on the line you are on

" highlights searches
set hlsearch
" bind turns off the highlighting until the next search
map <F12> :nohlsearch

hi Search ctermbg=0 ctermfg=7
" hi Search ctermbg=7 ctermfg=1

" tab stuff
set expandtab " tab keystroke changed to shiftwidth spaces
set tabstop=4 " display this many spaces for tab character
set softtabstop=4 " Remove 4 spaces once on backspace
set shiftwidth=4 " this many spaces for expandtab

" status line
set statusline=%F%m%r%h%w\ [%{&ff}]\ [type=%Y]\ [POS=%l,%v]\ [%p%%\/%L]\ [char=\%03.3b\|\%02.2B]
set laststatus=2

set title
set scrolloff=3

" my color prefs
hi Comment ctermfg=darkgrey
set background=light

